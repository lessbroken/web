all: collect publish

collect:
	build/collect_all.sh
	build/collect_static.sh

publish:
	build/publish_all.sh
