---
title: lessbroken.org
footer-messages:
    - The content on this page is released into the public domain, under [CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/).
---

# lessbroken.org

Hi, you've reached lessbroken.org. This system runs email, Git, and web hosting services. [Fox
Wilson](/~fwilson/) maintains lessbroken.org.

## Contact

You can contact Fox at `fwilson@lessbroken.org`, or on [freenode IRC](https://freenode.net) by
sending a PM to `fwilson`.

## Git

Public and private Git repository hosting is offered at
[dev.lessbroken.org](https://dev.lessbroken.org). If you are interested in an account, contact
Fox.

## Email

This server offers email services with Postfix and Dovecot. If you'd like an account, contact Fox.

If you already have one, you can [manage your account](/mailctl/). If you have an account creation
token, you can [create your account](/mailctl/create/).

A few important notes:

- If you are receiving abusive email from this system, send an email to `abuse@lessbroken.org`.
  All abuse reports are read and responded to, and accounts that are causing abusive activity will
  be promptly disabled.
- Opportunistic TLS is enabled. When you exchange emails with another provider that supports TLS,
  emails will be encrypted in transit.
- There is no webmail access. You will need to use an email client that supports IMAP and SMTP.
- End-to-end encryption using [GnuPG](https://gnupg.org) or similar software is *strongly*
  recommended.

## XMPP

This server offers XMPP/Jabber connectivity with Prosody, serving the <code>lessbroken.org</code>
domain. Registration is currently open to everyone. Abuse can be reported to
<code>abuse@lessbroken.org</code>.

Additionally, XMPP multi-user chats/conferences can be hosted on
<code>conference.lessbroken.org</code>.
