---
title: lessbroken.org CSS usage
footer-messages:
    - The content on this page is released into the public domain, under [CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/).
---

# CSS usage

You're more than welcome to copy and use the stylesheets that lessbroken.org uses. They're released
into the public domain (CC0), so there are no usage conditions.

That being said, I'd appreciate credit (although, again, it's not required). You can credit me by
putting a link like this: [Stylesheet credit](https://lessbroken.org/~fwilson) in your page footer,
comments, about section, or similar.

If you aren't using `/pref.css` as your stylesheet, please download and rehost `light.css` or
`dark.css` as appropriate. It's faster for you (and less costly for me) to host stylesheets from
your own server.
